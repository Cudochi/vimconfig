" Attention : Ce plugin et DAP ne devraient pas être activés ensembles !

" let g:nvimgdb_config_override = {
"       \ 'key_continue':   '<f8>',
"       \ 'key_next':       '<f5>',
"       \ 'key_step':       '<f6>',
"       \ 'key_finish':     '<f7>',
"       \ 'key_breakpoint': '<f9>',
"       \ }

" let g:nvimgdb_config = {
"     \ 'key_until':      '<f4>',
"     \ 'key_continue':   '<f5>',
"     \ 'key_next':       '<f10>',
"     \ 'key_step':       '<f11>',
"     \ 'key_finish':     '<f12>',
"     \ 'key_breakpoint': '<f8>',
"     \ 'key_frameup':    '<c-p>',
"     \ 'key_framedown':  '<c-n>',
"     \ 'key_eval':       '<f9>',
"     \ 'key_quit':       '<f2>',
"     \ 'set_tkeymaps':   'function("lua", "NvimGdb.i().keymaps:set_t()")',
"     \ 'set_keymaps':    'function("lua", "NvimGdb.i().keymaps.set()")',
"     \ 'unset_keymaps':  'function("lua", "NvimGdb.i().keymaps.unset()")',
"     \ 'sign_current_line': '▶',
"     \ 'sign_breakpoint': [ '●', '●²', '●³', '●⁴', '●⁵', '●⁶', '●⁷', '●⁸', '●⁹', '●ⁿ' ],
"     \ 'sign_breakpoint_priority': 10,
"     \ 'termwin_command': 'belowright new',
"     \ 'codewin_command': 'new',
"     \ 'set_scroll_off': 5,
"     \ 'jump_bottom_gdb_buf': v:true,
"     \ }

" nnoremap <F3> :GdbStart gdb 
" nnoremap <F4> :Gdb run 
" nnoremap <F10> :Gdb 

" nnoremap <F5> :GdbNext<Cr>
" nnoremap <F6> :GdbStep<Cr>
" nnoremap <F7> :GdbFinish<Cr>
" nnoremap <F8> :GdbContinue<Cr>
" nnoremap <F9> :GdbBreakpointToggle<Cr>
