lua << EOF
vim.opt.list = true
-- vim.opt.listchars:append "eol:↴"

require("indent_blankline").setup {
    char = "¦",
    context_char = '¦',
    show_current_context = true,
    show_current_context_start = true,
    show_trailing_blankline_indent = true,
    show_end_of_line = false,
    use_treesitter = false
}

EOF
