let g:VM_maps = {}
let g:VM_leader = '\'

let g:VM_mouse_mappings = 1
nmap   <C-LeftMouse>         <Plug>(VM-Mouse-Cursor)
nmap   <C-RightMouse>        <Plug>(VM-Mouse-Word)  
nmap   <M-C-RightMouse>      <Plug>(VM-Mouse-Column)

let g:VM_maps['Add Cursor at Position'] = '<C-n>'
