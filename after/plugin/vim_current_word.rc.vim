" Twins of word under cursor:
let g:vim_current_word#highlight_twins = 1
" The word under cursor:
let g:vim_current_word#highlight_current_word = 0

hi CurrentWordTwins gui=underline cterm=underline
hi CurrentWord gui=underline cterm=underline
let g:vim_current_word#highlight_delay = 500

autocmd BufEnter *Tree*,*bin/bash*,*toggleterm*,NvimTree*,NERD_tree_* :let b:vim_current_word_disabled_in_this_buffer = 1
" autocmd BufEnter *Tree*,NvimTree*,NERD_tree_*,term* :let b:vim_current_word_disabled_in_this_buffer = 1
