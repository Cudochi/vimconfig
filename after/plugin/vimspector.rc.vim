let g:vimspector_enable_mappings = 'HUMAN'

nmap <Leader>ww <Plug>VimspectorBalloonEval

nmap <F3> :VimspectorReset<Cr>
nmap <F5> <Plug>VimspectorStepOver
nmap <F6> <Plug>VimspectorStepInto
nmap <F7> <Plug>VimspectorStepOut
nmap <F8> <Plug>VimspectorContinue

let g:vimspector_sign_priority = {
  \    'vimspectorBP':         11,
  \    'vimspectorBPCond':     11,
  \    'vimspectorBPLog':      11,
  \    'vimspectorBPDisabled': 11,
  \    'vimspectorPC':         999,
  \ }

" let g:vimspector_sidebar_width = 75
" let g:vimspector_bottombar_height = 5

function! s:CustomiseUI()
  " Customise the basic UI...

  " Close the output window
  call win_gotoid( g:vimspector_session_windows.output )
  q
endfunction

augroup MyVimspectorUICustomistaion
  autocmd!
  autocmd User VimspectorUICreated call s:CustomiseUI()
augroup END
