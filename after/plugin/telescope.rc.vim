if !exists('g:loaded_telescope') | finish | endif

lua << EOF
local telescope = require('telescope')
local actions = require('telescope.actions')
local builtin = require('telescope.builtin')
local telescopeConfig = require("telescope.config")

-- Clone the default Telescope configuration
local vimgrep_arguments = { unpack(telescopeConfig.values.vimgrep_arguments) }

-- I want to search in hidden/dot files.
-- table.insert(vimgrep_arguments, "--hidden")
table.insert(vimgrep_arguments, "--no-ignore")
-- I don't want to search in the `.git` directory.
table.insert(vimgrep_arguments, "--glob")
table.insert(vimgrep_arguments, "!.git/*")

telescope.setup{
    defaults = {
        -- Default configuration for telescope goes here:
        -- config_key = value,
        file_ignore_patterns = { },
		-- `hidden = true` is not supported in text grep commands.
		vimgrep_arguments = vimgrep_arguments,
        mappings = {
            n = {
                -- map actions.which_key to <C-h> (default: <C-/>)
                -- actions.which_key shows the mappings for your picker,
                -- e.g. git_{create, delete, ...}_branch for the git_branches picker
                ["k"] = actions.move_selection_next,
                ["i"] = actions.move_selection_previous,
                ["dd"] = require('telescope.actions').delete_buffer,
                -- ["j"] = "which_key"
                -- ["l"] = "which_key"
            }
        }
    },

    pickers = {
        -- Default configuration for builtin pickers goes here:
        -- picker_name = {
        --   picker_config_key = value,
        --   ...
        -- }
        -- Now the picker_config_key will be applied every time you call this
        -- builtin picker
        -- builtin.builtin {include_extensions = true}
        find_files = {
			-- `hidden = true` will still show the inside of `.git/` as it's not `.gitignore`d.
			find_command = { "rg", "--files", "--hidden", "--glob", "!.git/*" },
            -- {hidden}           (boolean)         determines whether to show hidden
            --                                      files or not (default: false)
            -- {no_ignore}        (boolean)         show files ignored by .gitignore,
            --                                      .ignore, etc. (default: false)
            hidden = true,
            no_ignore = true,
            },
        fd = {
			-- `hidden = true` will still show the inside of `.git/` as it's not `.gitignore`d.
			find_command = { "rg", "--files", "--hidden", "--glob", "!.git/*" },
            -- {hidden}           (boolean)         determines whether to show hidden
            --                                      files or not (default: false)
            -- {no_ignore}        (boolean)         show files ignored by .gitignore,
            --                                      .ignore, etc. (default: false)
            hidden = true,
            no_ignore = true,
            },
        live_grep = {
                additional_args = function() return {
                    "-u",
                    "-F",
                    "--max-depth=99",
                    -- "grep_open_files=true",
                } end
            },
        grep_string = {
                additional_args = function() return {
                    -- no_ignore = true,
                    -- hidden= true,
                    -- "grep_open_files=true",
                } end
            },
    },

    extensions = {
        -- Your extension configuration goes here:
        -- extension_name = {
        --   extension_config_key = value,
        -- }
        -- please take a look at the readme of the extension you want to configure
        fzf = {
            fuzzy = true,                   -- false will only do exact matching
            override_generic_sorter = true, -- override the generic sorter
            override_file_sorter = true,    -- override the file sorter
            case_mode = "smart_case",       -- or "ignore_case" or "respect_case"
                                            -- the default case_mode is "smart_case"
        },
        -- coc = {
        --     theme = 'ivy',
        --     prefer_locations = true, -- always use Telescope locations to preview definitions/declarations/implementations etc
        -- },
        undo = {
            mappings = {
                n = {
                    -- IMPORTANT: Note that telescope-undo must be available when telescope is configured if
                    -- you want to replicate these defaults and use the following actions. This means
                    -- installing as a dependency of telescope in it's `requirements` and loading this
                    -- extension from there instead of having the separate plugin definition as outlined
                    -- above.
                    -- ["<cr>"] = require("telescope-undo.actions").yank_additions,
                    -- ["<S-cr>"] = require("telescope-undo.actions").yank_deletions,
                    ["<cr>"] = require("telescope-undo.actions").restore,
                },
            },
        },
        -- media_files = {
        --     -- filetypes whitelist
        --     -- defaults to {"png", "jpg", "mp4", "webm", "pdf"}
        --     filetypes = {"png", "webp", "jpg", "jpeg"},
        --     find_cmd = "find" -- find command (defaults to `fd`)   
        -- },
    }
}

-- To get fzf loaded and working with telescope, you need to call
-- load_extension, somewhere after setup function:
require('telescope').load_extension('fzf')
require('telescope').load_extension('vim_bookmarks')
require('telescope').load_extension('yabs')
-- require('telescope').load_extension('projects')
-- require('telescope').load_extension('coc')
require("telescope").load_extension("live_grep_args")
require("telescope").load_extension("session-lens")
require("telescope").load_extension("ctags_plus")
-- require("telescope").load_extension("dap")
require("telescope").load_extension("vimspector")
require("telescope").load_extension("undo")
-- require('telescope').load_extension('media_files')
-- builtin.builtin { include_extensions = true }
EOF
