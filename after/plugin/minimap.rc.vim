""""""""""""""""""""""""""""""""""""""
" --- minimap.vim section ---
""""""""""""""""""""""""""""""""""""""

"let g:minimap_width = 5 
"let g:minimap_auto_start = 1

let g:minimap_highlight_range = 1
let g:minimap_git_colors = 1

hi MinimapCurrentLine ctermfg=Yellow guifg=#50FA7B guibg=Yellow
let g:minimap_highlight = 'Substitute'
