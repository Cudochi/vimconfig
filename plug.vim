""""""""""""""""""""""""""""""""""""""
" --- Vundle section ---
""""""""""""""""""""""""""""""""""""""

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.config/nvim/bundle/Vundle.vim
call vundle#begin('~/.config/nvim/bundle/')
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Keep Plugin commands between vundle#begin/end.
" ---

Plugin 'dracula/vim', { 'name': 'dracula' }
"Plugin for the colorscheme Dracula.

Plugin 'sainnhe/sonokai'
"Plugin for the sonokai colorscheme.

Plugin 'arcticicestudio/nord-vim'
"Plugin for the nord colorscheme.

Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html

Plugin 'tpope/vim-surround'
" Delete/change/add parentheses/quotes/XML-tags/much more with ease 

Plugin 'airblade/vim-gitgutter'
" For git modifications

Plugin 'vim-airline/vim-airline'
" Advanced status line

Plugin 'vim-airline/vim-airline-themes'
" Dependency for airline

"Plugin 'townk/vim-autoclose'
" Autoclose brackets of any type.
" Deprecated

Plugin 'Raimondi/delimitMate'
" New plugin for autoclosing brackets.

" Plugin 'windwp/nvim-autopairs'
" New new plugin for autoclosing brackets.

" Plugin 'Yggdroot/indentLine'
" A vim plugin to display the indention levels with thin vertical lines

Plugin 'lukas-reineke/indent-blankline.nvim'
" Indent guides for Neovim

Plugin 'numToStr/Comment.nvim'
" Smart and powerful comment plugin for neovim.

Plugin 'MattesGroeger/vim-bookmarks'
" Vim bookmark plugin.

"Plugin 'ctrlpvim/ctrlp.vim'
" Active fork of kien/ctrlp.vim—Fuzzy file, buffer, mru, tag, etc finder. 

" Plugin 'nvim-lua/popup.nvim'
Plugin 'nvim-lua/plenary.nvim'
Plugin 'nvim-telescope/telescope.nvim'
Plugin 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }
Plugin 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plugin 'p00f/nvim-ts-rainbow'
Plugin 'nvim-treesitter/nvim-treesitter-context'
" Context.vim a une meilleure présentation
Plugin 'tom-anders/telescope-vim-bookmarks.nvim'
Plugin 'nvim-telescope/telescope-live-grep-args.nvim'
Plugin 'rmagatti/session-lens'
Plugin 'gnfisher/nvim-telescope-ctags-plus'
" Plugin 'nvim-telescope/telescope-dap.nvim'
Plugin 'nvim-telescope/telescope-vimspector.nvim'
Plugin 'debugloop/telescope-undo.nvim'
" A session-switcher extension for rmagatti/auto-session using Telescope.nvim 
" Plugin 'fannheyward/telescope-coc.nvim'
" Plugin 'nvim-telescope/telescope-media-files.nvim'
" Buggy.
Plugin 'kyazdani42/nvim-web-devicons'
" Find, Filter, Preview, Pick. All lua, all the time.

Plugin 'windwp/nvim-spectre'
" Search and replace.

Plugin 'wfxr/minimap.vim'
" Blazing fast minimap / scrollbar for vim, powered by code-minimap written in Rust.

" Plugin 'psliwka/vim-smoothie'
"Smooth scrooling from screen to screen.


" ---------------------------------------

  " LSP Support
  Plugin 'neovim/nvim-lspconfig'             " Required
  Plugin 'williamboman/mason.nvim'           " Optional
  Plugin 'williamboman/mason-lspconfig.nvim' " Optional

  " Autocompletion Engine
  Plugin 'hrsh7th/nvim-cmp'         " Required
  Plugin 'hrsh7th/cmp-nvim-lsp'     " Required
  Plugin 'hrsh7th/cmp-buffer'       " Optional
  Plugin 'hrsh7th/cmp-path'         " Optional
  Plugin 'saadparwaiz1/cmp_luasnip' " Optional
  Plugin 'hrsh7th/cmp-nvim-lua'     " Optional

  " Snippets
  Plugin 'L3MON4D3/LuaSnip'             " Required
  Plugin 'rafamadriz/friendly-snippets' " Optional

  Plugin 'onsails/lspkind-nvim'
  Plugin 'rmagatti/goto-preview'
  Plugin 'glepnir/lspsaga.nvim'

  " LSP Setup
  Plugin 'VonHeikemen/lsp-zero.nvim', {'branch': 'v1.x'}

" ---------------------------------------

" Plugin 'neovim/nvim-lspconfig'
" "Configuration for the native LSP (Language Support Server).
"
" Plugin 'williamboman/mason.nvim'
" Plugin 'williamboman/mason-lspconfig.nvim'
"
" Plugin 'hrsh7th/nvim-cmp'
" Plugin 'hrsh7th/cmp-buffer'
" Plugin 'hrsh7th/cmp-path'
" Plugin 'hrsh7th/cmp-nvim-lsp'
" Plugin 'hrsh7th/cmp-vsnip'
" Plugin 'hrsh7th/vim-vsnip'
" Plugin 'hrsh7th/cmp-nvim-lua'
"
" "  Snippets
" Plugin 'L3MON4D3/LuaSnip'
" Plugin 'rafamadriz/friendly-snippets'
"
" Plugin 'onsails/lspkind-nvim'

" ---------------------------------------

Plugin 'puremourning/vimspector'
" A multi-language debugging system for Vim

" Plugin 'mfussenegger/nvim-dap'
" Plugin 'Pocco81/DAPInstall.nvim'
" Plugin 'rcarriga/nvim-dap-ui'
" Plugin 'theHamsta/nvim-dap-virtual-text'
" Alternative to vimspector. Doesn't work as well.

" Plugin 'sakhnik/nvim-gdb'
" Neovim thin wrapper for GDB, LLDB, PDB/PDB++ and BashDB 

Plugin 'iamcco/markdown-preview.nvim'
" Markdown preview plugin for (neo)vim

Plugin 'kyazdani42/nvim-tree.lua'
" A file explorer tree for neovim written in lua

" Plugin 'ms-jpq/chadtree', {'branch': 'chad', 'do': 'python3 -m chadtree deps'}
" File manager for Neovim. Better than NERDTree.

Plugin 'ldelossa/litee.nvim'
Plugin 'ldelossa/litee-calltree.nvim'
Plugin 'ldelossa/litee-symboltree.nvim'
Plugin 'ldelossa/litee-filetree.nvim'
Plugin 'ldelossa/litee-bookmarks.nvim'
" Neovim's missing IDE features 

" Plugin 'petertriho/nvim-scrollbar'
" Extensible Neovim Scrollbar

" Plugin 'akinsho/bufferline.nvim', { 'tag': '*' }
Plugin 'akinsho/toggleterm.nvim'

" Plugin 'simrat39/symbols-outline.nvim'
" A tree like view for symbols in Neovim using the Language Server Protocol. Supports all your favourite languages. 

" Plugin 'sidebar-nvim/sidebar.nvim'

Plugin 'pianocomposer321/yabs.nvim'
" Yet Another Build System/Code Runner for Neovim, written in lua 

" Plugin 'm-pilia/vim-ccls'

Plugin 'folke/which-key.nvim'

" Plugin 'neoclide/coc.nvim', {'branch': 'release'}
" Nodejs extension host for vim & neovim, load extensions like VSCode and host language servers.

Plugin 'tpope/vim-repeat'
" repeat.vim: enable repeating supported plugin maps with "."

Plugin 'ggandor/leap.nvim'
" Next-generation motion plugin using incremental input processing, allowing for unparalleled speed with minimal cognitive effort 
" Neovim's answer to the mouse

" Plugin 'wellle/context.vim'
" Vim plugin that shows the context of the currently visible buffer contents 

Plugin 'sbdchd/neoformat'
" A (Neo)vim plugin for formatting code.

" Plugin 'ahmedkhalf/project.nvim'
" The superior project management solution for neovim.

Plugin 'romainchapou/confiture.nvim'
" A neovim lua plugin to save and launch project specific commands.

Plugin 'famiu/bufdelete.nvim'
" Delete Neovim buffers without losing window layout 

" Plugin 'mg979/vim-visual-multi'
" Multi cursor ?

Plugin 'gpanders/editorconfig.nvim'
" EditorConfig plugin for Neovim

Plugin 'roxma/nvim-yarp'
Plugin 'roxma/vim-hug-neovim-rpc'
Plugin 'gelguy/wilder.nvim'
" A more adventurous wildmenu 
" Useless

Plugin 'EdenEast/nightfox.nvim'
" Colorscheme

Plugin 'daschw/leaf.nvim'
" Colorscheme

Plugin 'catppuccin/nvim', { 'as': 'catppuccin' }
" Colorscheme

Plugin 'Tsuzat/NeoSolarized.nvim', { 'branch': 'master' }
" Colorscheme

Plugin 'folke/tokyonight.nvim' 
" Colorscheme

Plugin 'rose-pine/neovim'
" Colorscheme

Plugin 'sindrets/diffview.nvim'
" Single tabpage interface for easily cycling through diffs for all modified
" files for any git rev.

Plugin 'rmagatti/auto-session'
" A small automated session manager for Neovim 

Plugin 'ludovicchabant/vim-gutentags'
" A Vim plugin that manages your tag files 

Plugin 'Pocco81/auto-save.nvim'
" Automatically save your changes in NeoVim

Plugin 'svermeulen/vim-cutlass'
" Plugin that adds a 'cut' operation separate from 'delete'

Plugin 'karb94/neoscroll.nvim'
" Smooth scrolling neovim plugin written in lua.

Plugin 'dominikduda/vim_current_word'
"  Plugin highlighting word under cursor and all of its occurences.

Plugin 'nacro90/numb.nvim'
" Peek lines just when you intend

Plugin 'NMAC427/guess-indent.nvim'
" Automatic indentation style detection for Neovim

" Plugin 'luukvbaal/statuscol.nvim'
"
" Plugin 'kevinhwang91/promise-async'
" " " Dependency for nvim-ufo
"
" Plugin 'kevinhwang91/nvim-ufo'
" " Not UFO in the sky, but an ultra fold in Neovim.
" " Trop tôt pour implémenter ça.

Plugin 'chaoren/vim-wordmotion'
" More useful word motions for Vim
" testtest_testtestTTtest

Plugin 'lewis6991/impatient.nvim'
" Improve startup time for Neovim

" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}

" ---
" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" This plugin is loaded as soon as possible to load the cache for the others plugins.
lua require('impatient')
