# Configuration Vim personnelle.

## Instruction d'installation :

1. Installer Neovim Nightly (0.7)
2. Installer :
    * Une police patchée NERDFont
    * Installer tout le reste :grimacing:

## ToDo :

- [ ] Gérer le copier/coller entre NeoVim et Linux.

- [ ] Selection des fonctions en bloc.

- [X] Commenter des blocs de code facilement.

- [ ] Indenter des blocs de code facilement.

- [X] Pouvoir replier du code VimScript.

- [X] Supprimer des mots même sans avoir le curseur sur leur premier caractère.
