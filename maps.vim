""""""""""""""""""""""""""""""""""""""
" --- Remap section ---
""""""""""""""""""""""""""""""""""""""

" Set the leader key.
let mapleader = " "

" Remap so when a piece of snippet is selected for replacement, typing ihjk
" will actually type ihjk instead of moving the selection.
snoremap i i
snoremap h h
snoremap j j
snoremap k k

xnoremap i k
xnoremap k j
xnoremap j h
xnoremap h i

" Remap the first degree displacements keys.
" Displacement between letters.
" nmap i <Up>
" nmap j <Left>
" nmap k <Down>
" noremap h i
nnoremap i k
nnoremap k j
nnoremap j h
nnoremap h i

" Remap the second degree displacement keys.
" Displacement between words and paragraphs.
noremap <S-j> b
noremap <S-i> {
noremap <S-k> }
noremap <S-l> e

" Remap the third degree displacement keys.
" Displacement between frames.
"execute "set <M-j>=\ej"
noremap <M-j> <C-w>h
"execute "set <M-l>=\el"
noremap <M-l> <C-w>l
"execute "set <M-i>=\ei"
noremap <M-i> <C-w>k
"execute "set <M-k>=\ek"
noremap <M-k> <C-w>j

" Jump backward into the jumplist
noremap <C-p> <C-i>

"noremap <C-j> gT
noremap <C-j> :bp<Cr>
"noremap <C-l> gt
noremap <C-l> :bn<Cr>
" Commented to use the neoscroll plugin instead.
" noremap <C-i> <C-b>
" noremap <C-k> <C-f>

" Smoothie.vim keybinds : (obsolete)
"noremap <C-i> <cmd>call smoothie#backwards()<CR>
"noremap <C-k> <cmd>call smoothie#forwards()<CR>

" I no longer know what it is, but it can't be good.
"tnoremap <Esc> <Esc> <C-w>N

" Remap <Esc> to exit the terminal
tnoremap <Esc> <C-\><C-n>

"Prevent the cursor form going back when exiting the insert mode.
inoremap <silent> <Esc> <Esc>`^

"Useless...
"nmap <silent> <C-_> <Plug>(pydocstring)

" Goto the beginning and end of a line.
nnoremap ù ^
nnoremap $ *
nnoremap * $

" Goto the beginning of the end of a file.
" nmap e G
" nmap E gg

" Remap <Down> to something else in the cmdline.
cmap <A-a> <Down>

" noremap <silent> <Leader>j& <cmd>lua require('lspsaga.hover').render_hover_doc()<CR>
" noremap <silent> <Leader>jé :Lspsaga lsp_finder<CR>
" noremap <silent> <Leader>j( :Lspsaga signature_help<CR>
" noremap <silent> <Leader>j" :Lspsaga show_cursor_diagnostics<CR>
" noremap <silent> <Leader>j- <cmd>lua vim.lsp.buf.implementation()<CR>
" noremap <silent> <Leader>j' :lua vim.lsp.buf.definition()<CR>


" Toggle the opening of the minimap.
noremap <Leader>) :MinimapToggle<Cr>

" Toggle the sidebar (multifunction)
noremap <Leader>_ :SidebarNvimToggle<Cr>

" Toggle the symbol-only sidebar
" noremap <Leader>_ :SymbolsOutline<Cr>
" Toggle the terminal
noremap <Leader>à :ToggleTerm<Cr>

" Toggle NvimTree
noremap <Leader>ç :NvimTreeToggle<Cr>

"Ccls commands
" noremap <Leader>f& :CclsCallHierarchy<Cr>
" noremap <Leader>fé :CclsMemberHierarchy<Cr>

" Open Telescope
nmap <C-h> :Telescope builtin include_extensions=true<Cr>
nmap <Leader>t :Telescope builtin include_extensions=true<Cr>
nmap <Leader>l :Telescope live_grep<Cr>
" nmap <Leader>hf :lua require'telescope.builtin'.live_grep(require('telescope.themes').get_ivy({}))<cr>
nmap <Leader>s <cmd>:lua require('telescope.builtin').grep_string()<Cr>
" nmap <Leader>hd <cmd>:lua require('telescope.builtin').grep_string({no_ignore="true", hidden="true"})<Cr>
nmap <Leader>v :Telescope vim_bookmarks<Cr>
nnoremap <Leader>j² <cmd>lua require('telescope').extensions.ctags_plus.jump_to_tag({initial_mode="normal"})<cr>
nnoremap B <cmd>lua require('telescope.builtin').buffers({initial_mode="normal"})<cr>

" Remap of the buffer delete command
:ca bd Bdelete

" Make the indentation of blocks of code not shitty.
xnoremap <Tab> >gv
xnoremap <S-Tab> <gv

" Remap the center screen command
nmap <C-u> zz

" Tab in normal mode
nnoremap <Tab> >>_
nnoremap <S-Tab> <<_

" Shortcut to make
" Remplacé par Confiture.nvim
nnoremap <Leader>mm :w <bar> make!<Cr>

" Map to save with <Ctrl> and <Leader>
nnoremap <C-s> :w<Cr>
" nnoremap <Leader>s :w<Cr>

" Map <Leader>q to :q for convenience
nnoremap <Leader>q :q<Cr>

" Map <Leader>Q to :bd for convenience
nnoremap <Leader>Q :Bdelete<Cr>

" Delete from the beginning of the line to the cursor, then goes into insert
" mode
nnoremap dù c^
" Delete from cursor to end of line, then goes into insert mode
nnoremap d* c$

" Better fold keybinds
" nnoremap <Leader>fa za
" nnoremap <Leader>fc zM
" nnoremap <Leader>fo zR
nnoremap zm zM
nnoremap zr zR

" Tout selectionner
nnoremap <C-a> ggVG

" Copier avec <C-c>
xnoremap <C-c> y


" Pour vim-cutlass
nnoremap m d
xnoremap m d

nnoremap mm dd
nnoremap M D

nnoremap p P

nnoremap <silent> <Esc> :noh<Cr>

" To toggle the relativenumber
nnoremap <Leader>r :set relativenumber!<Cr>

" Prevent the commandline panel from opening.
nnoremap q: <Nop>
