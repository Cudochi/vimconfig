""""""""""""""""""""""""""""""""""""""
" Maintainer : Cudochi
" https://gitlab.com/Cudochi/vimconfig
""""""""""""""""""""""""""""""""""""""

" Imports plugins
runtime ./plug.vim
" Configure the colorscheme
runtime ./colorscheme.vim

""""""""""""""""""""""""""""""""""""""
" --- Neovide section ---
""""""""""""""""""""""""""""""""""""""

" set termguicolors
" let g:neovide_transparency = 0.75
" let g:neovide_cursor_animation_length=0

""""""""""""""""""""""""""""""""""""""
" --- Customization section ---
""""""""""""""""""""""""""""""""""""""

set virtualedit=onemore
" set shortmess=I
set encoding=UTF-8
set number

set tabstop=4
set shiftwidth=4
set autoindent
set cindent
set expandtab
set softtabstop=0
set smarttab
" trigger `autoread` when files changes on disk
  set autoread
  autocmd FocusGained,BufEnter,CursorHold,CursorHoldI * if mode() != 'c' | checktime | endif
" notification after file change
  autocmd FileChangedShellPost *
    \ echohl WarningMsg | echo "File changed on disk. Buffer reloaded." | echohl None

set splitbelow
set splitright
set mouse=a
set showcmd
set clipboard+=unnamedplus
set laststatus=3
set wildoptions+=pum
" turn hybrid line numbers on
set number relativenumber
set nu rnu
let g:gitgutter_map_keys = 0
let g:bookmark_save_per_working_dir = 1

" let g:molokai_original = 1
" let g:rehash256 = 1
" let g:sonokai_transparent_background = 1
set backspace=indent,eol,start
" let g:indentLine_char = '│'
" let g:indentLine_setColors = 0
" let g:indentLine_defaultGroup = 'Comment'
" let g:indentLine_enabled = 1
autocmd Filetype json let g:indentLine_setConceal = 0
" There is a space at the end of the line.
" set list lcs=tab:\¦\ 

syntax on
hi Normal guibg=NONE ctermbg=NONE
" highlight Pmenu ctermfg=7 ctermbg=236 guifg=#000000 guibg=#000000
" highlight PmenuSel ctermfg=11 ctermbg=241 guifg=#000000 guibg=#000000
set foldmethod=indent
set foldlevel=9
" set foldnestmax=20

let g:airline#extensions#tabline#enabled=1
let g:airline#extensions#coc#enabled=1
let g:airline#extensions#coc#show_coc_status = 1
let g:airline_theme='distinguished'
let g:airline_powerline_fonts=1
let g:airline#extensions#whitespace#enabled = 0
let g:airline#extensions#branch#format = 2
"let g:airline#extensions#branch#displayed_head_limit = 10
"let g:airline_skip_empty_sections = 1
" let g:airline#extensions#tabline#enabled = 0

highlight clear signcolumn
"set signcolumn=number
set timeoutlen=300
set ttimeoutlen=50
set updatetime=100
" autocmd BufWinEnter,WinEnter term://* set timeoutlen=100
" autocmd BufLeave term://* set timeoutlen=500
set whichwrap+=<,>,h,l,[,]
" set guicursor=a:block-blinkwait700-blinkon400-blinkoff250
set guicursor=a:block-blinkwait700-blinkon400-blinkoff250,i-r-c-ci-cr:ver10-blinkwait700-blinkon400-blinkoff250
" set guicursor=a:ver10-blinkwait700-blinkon400-blinkoff250
"let g:pydocstring_doq_path="/home/kavan/.local/bin/doq"
"let g:semshi#error_sign=0

"set fillchars+=vert:\|
hi! vertsplit gui=NONE cterm=NONE 

" Persistent undo setup
" guard for distributions lacking the 'persistent_undo' feature.
if has('persistent_undo')
    " define a path to store persistent undo files.
    let target_path = expand('~/.local/share/nvim/persistent-undo/')    " create the directory and any parent directories
    " if the location does not exist.
    if !isdirectory(target_path)
        call system('mkdir -p ' . target_path)
    endif    " point Vim to the defined undo directory.
    let &undodir = target_path    " finally, enable undo persistence.
    set undofile
endif

""""""""""""""""""""""""""""""""""""""
" --- Session ---
""""""""""""""""""""""""""""""""""""""

" Disable the autosaving fuction of the plugin.
" let g:session_autosave = 'no'
" let g:session_directory = '~/.config/nvim/sessions'
" Name of the file minus the filetype extention in ~/.vim/sessions/
" let g:session_default_name = 'default_13'
" Autoload the default file session.
" let g:session_autoload = 'yes'

" " lua << EOF
" " function NvimTreeCloseIfLast()
" "   local only_one = vim.fn.tabpagenr('$') == 1 and vim.fn.winnr('$') == 1
" "   local is_visible = require('nvim-tree.view').is_visible()
" "
" "   if only_one and is_visible then
" "     vim.cmd('quit')
" "   end
" " end
" "
" " vim.cmd('autocmd BufEnter * lua NvimTreeCloseIfLast()')
" " EOF

autocmd BufEnter * ++nested if winnr('$') == 1 && bufname() == 'NvimTree_' . tabpagenr() | quit | endif
" let g:auto_session_post_restore_cmds = ["NvimTreeToggle", "wincmd l"]

""""""""""""""""""""""""""""""""""""""
" --- Remap section ---
""""""""""""""""""""""""""""""""""""""

" Import Vimspector configuration
runtime ./after/plugin/vimspector.rc.vim

" Import Comment.nvim configuration
" runtime ./after/plugin/comment.rc.vim

" Imports custom mapping
runtime ./maps.vim
