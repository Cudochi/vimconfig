" ~/.config/nvim/sessions/fastapi_small_app_4.vim:
" Vim session script.
" Created by session.vim 2.13.1 on 09 juillet 2021 at 18:48:38.
" Open this file in Vim and run :source % to restore your session.

if exists('g:syntax_on') != 1 | syntax on | endif
if exists('g:did_load_filetypes') != 1 | filetype on | endif
if exists('g:did_load_ftplugin') != 1 | filetype plugin on | endif
if exists('g:did_indent_on') != 1 | filetype indent on | endif
if &background != 'dark'
	set background=dark
endif
call setqflist([])
let SessionLoad = 1
let s:so_save = &so | let s:siso_save = &siso | set so=0 siso=0
let v:this_session=expand("<sfile>:p")
silent only
cd ~/Documents/projects/Internship/tachotoweb
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
badd +78 src/utils/signpdf.py
badd +134 ~/.config/nvim/init.vim
badd +1157 term://.//196556:/bin/bash
badd +38 .gitignore
badd +4 term://.//197475:/bin/bash
badd +29 src/utils/pdfSign.py
badd +64 src/modules/pdf_editing_mod.py
badd +16 src/main.py
badd +72 src/modules/pdf_handling_mod.py
badd +1 src/modules/
argglobal
%argdel
$argadd src/utils/signpdf.py
set stal=2
edit src/main.py
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
wincmd _ | wincmd |
split
1wincmd k
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
wincmd w
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
exe 'vert 1resize ' . ((&columns * 31 + 106) / 213)
exe '2resize ' . ((&lines * 42 + 29) / 58)
exe 'vert 2resize ' . ((&columns * 90 + 106) / 213)
exe '3resize ' . ((&lines * 42 + 29) / 58)
exe 'vert 3resize ' . ((&columns * 90 + 106) / 213)
exe '4resize ' . ((&lines * 12 + 29) / 58)
exe 'vert 4resize ' . ((&columns * 181 + 106) / 213)
argglobal
enew
" file NERD_tree_1
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=2
setlocal fml=1
setlocal fdn=20
setlocal nofen
wincmd w
argglobal
setlocal fdm=expr
setlocal fde=SimpylFold#FoldExpr(v:lnum)
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=2
setlocal fml=1
setlocal fdn=20
setlocal fen
let s:l = 16 - ((13 * winheight(0) + 21) / 42)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
16
normal! 023|
wincmd w
argglobal
if bufexists("src/modules/pdf_handling_mod.py") | buffer src/modules/pdf_handling_mod.py | else | edit src/modules/pdf_handling_mod.py | endif
setlocal fdm=expr
setlocal fde=SimpylFold#FoldExpr(v:lnum)
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=2
setlocal fml=1
setlocal fdn=20
setlocal fen
let s:l = 14 - ((12 * winheight(0) + 21) / 42)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
14
normal! 023|
wincmd w
argglobal
if bufexists("term://.//197475:/bin/bash") | buffer term://.//197475:/bin/bash | else | edit term://.//197475:/bin/bash | endif
setlocal fdm=indent
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=2
setlocal fml=1
setlocal fdn=20
setlocal fen
let s:l = 126 - ((11 * winheight(0) + 6) / 12)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
126
normal! 082|
wincmd w
exe 'vert 1resize ' . ((&columns * 31 + 106) / 213)
exe '2resize ' . ((&lines * 42 + 29) / 58)
exe 'vert 2resize ' . ((&columns * 90 + 106) / 213)
exe '3resize ' . ((&lines * 42 + 29) / 58)
exe 'vert 3resize ' . ((&columns * 90 + 106) / 213)
exe '4resize ' . ((&lines * 12 + 29) / 58)
exe 'vert 4resize ' . ((&columns * 181 + 106) / 213)
tabedit src/modules/pdf_handling_mod.py
set splitbelow splitright
wincmd _ | wincmd |
vsplit
wincmd _ | wincmd |
vsplit
2wincmd h
wincmd w
wincmd w
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
exe 'vert 1resize ' . ((&columns * 31 + 106) / 213)
exe 'vert 2resize ' . ((&columns * 80 + 106) / 213)
exe 'vert 3resize ' . ((&columns * 100 + 106) / 213)
argglobal
enew
" file NERD_tree_1
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=2
setlocal fml=1
setlocal fdn=20
setlocal nofen
wincmd w
argglobal
setlocal fdm=expr
setlocal fde=SimpylFold#FoldExpr(v:lnum)
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=2
setlocal fml=1
setlocal fdn=20
setlocal fen
40
silent! normal! zo
72
silent! normal! zo
107
silent! normal! zo
118
silent! normal! zo
let s:l = 130 - ((49 * winheight(0) + 27) / 55)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
130
normal! 013|
wincmd w
argglobal
if bufexists("src/modules/pdf_editing_mod.py") | buffer src/modules/pdf_editing_mod.py | else | edit src/modules/pdf_editing_mod.py | endif
setlocal fdm=expr
setlocal fde=SimpylFold#FoldExpr(v:lnum)
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=2
setlocal fml=1
setlocal fdn=20
setlocal fen
33
silent! normal! zo
let s:l = 78 - ((51 * winheight(0) + 27) / 55)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
78
normal! 010|
wincmd w
2wincmd w
exe 'vert 1resize ' . ((&columns * 31 + 106) / 213)
exe 'vert 2resize ' . ((&columns * 80 + 106) / 213)
exe 'vert 3resize ' . ((&columns * 100 + 106) / 213)
tabnext 2
set stal=1
if exists('s:wipebuf') && getbufvar(s:wipebuf, '&buftype') isnot# 'terminal'
"   silent exe 'bwipe ' . s:wipebuf
endif
" unlet! s:wipebuf
set winheight=1 winwidth=20 winminheight=1 winminwidth=1 shortmess=filnxtToOFc
let s:sx = expand("<sfile>:p:r")."x.vim"
if file_readable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &so = s:so_save | let &siso = s:siso_save

" Support for special windows like quick-fix and plug-in windows.
" Everything down here is generated by vim-session (not supported
" by :mksession out of the box).

1wincmd w
tabnext 1
let s:bufnr_save = bufnr("%")
let s:cwd_save = getcwd()
NERDTree ~/Documents/projects/Internship/tachotoweb
if !getbufvar(s:bufnr_save, '&modified')
  let s:wipebuflines = getbufline(s:bufnr_save, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:bufnr_save
  endif
endif
execute "cd" fnameescape(s:cwd_save)
1resize 55|vert 1resize 31|2resize 42|vert 2resize 90|3resize 42|vert 3resize 90|4resize 12|vert 4resize 181|
1wincmd w
tabnext 2
let s:bufnr_save = bufnr("%")
let s:cwd_save = getcwd()
NERDTreeMirror
if !getbufvar(s:bufnr_save, '&modified')
  let s:wipebuflines = getbufline(s:bufnr_save, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:bufnr_save
  endif
endif
execute "cd" fnameescape(s:cwd_save)
1resize 55|vert 1resize 31|2resize 55|vert 2resize 80|3resize 55|vert 3resize 100|
2wincmd w
tabnext 2
if exists('s:wipebuf')
  if empty(bufname(s:wipebuf))
if !getbufvar(s:wipebuf, '&modified')
  let s:wipebuflines = getbufline(s:wipebuf, 1, '$')
  if len(s:wipebuflines) <= 1 && empty(get(s:wipebuflines, 0, ''))
    silent execute 'bwipeout' s:wipebuf
  endif
endif
  endif
endif
doautoall SessionLoadPost
unlet SessionLoad
" vim: ft=vim ro nowrap smc=128
